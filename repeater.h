#include <pthread.h>

typedef struct Channel_source_Type {
    int  port;
    unsigned char cancelation;
    struct Channel_source_Type* oposite_channel_source;
 } Channel_source;

 #define DataBufferSize 256*256
 #define MaxConsumersCount 256

 typedef struct Sink_Type {
    int  consumersCount;
    char ports[MaxConsumersCount][256];
    char lockedPorts[MaxConsumersCount]; //reserved
    char openedPorts[MaxConsumersCount]; //0 - not initialized, 1 - opened, 2 - closed
    long writeCount[MaxConsumersCount];
    long readCount[MaxConsumersCount][MaxConsumersCount];
    int writeOffset[MaxConsumersCount];
    int readOffset[MaxConsumersCount][MaxConsumersCount];
    char data[MaxConsumersCount][DataBufferSize];
 } Sink;

 struct termios ajust_stdio();

 int create_channel(int port, void * perform);

 int join_channels();

 void init_repeater(const char * port, const char * sink, int isInit);

 void* perform_listen( void* argument );

 char * getOSSpecificPortName(const char * port_name,  char * port);

 void create_input( void* argument );

 int openSerialPort(const char * port);

 int create_sink(const char * port, const char * sink, int isInit);

 void read_mem(char * buffer);

 void write_mem(const char * buffer);
 
 void destroy_sink();
