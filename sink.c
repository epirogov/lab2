#include <stdio.h> 
#include <stdlib.h> 
#include <sys/types.h> 
#include <sys/stat.h> 
#include <fcntl.h> 
#include <string.h> 
#include <unistd.h> 
#include <semaphore.h> 
#include <sys/mman.h>
#include "repeater.h"

//todo: should be rooted by sink name
#define SEM_MUTEX_NAME "/sem-mutex" 
#define SEM_BUFFER_COUNT_NAME "/sem-buffer-count" 
#define SEM_SPOOL_SIGNAL_NAME "/sem-spool-signal"

//todo: is max buffers neeeded?
#define MAX_BUFFERS 256

Sink * shared_mem_ptr;
sem_t *mutex_sem, *buffer_count_sem, *spool_signal_sem;
int fd_shm, fd_log;

int portNumber = -1;

int create_sink(const char * port, const char * sink, int isInit)
{
    // Get shared memory
    if ((fd_shm = shm_open (sink, O_RDWR, 0)) == -1)
    {
        if ((fd_shm = shm_open (sink, O_RDWR | O_CREAT, 0660)) == -1) 
            error ("shm_open");
        printf("Shared memory created %s\n", sink);
        isInit = 1;
    }
    else 
        printf("Shared memory opened %s\n", sink);

    // mutual exclusion semaphore, mutex_sem with an initial value 0. 
    if ((mutex_sem = sem_open (sink, O_CREAT, 0660, 0)) == SEM_FAILED) 
        error ("sem_open");
    
        // Get shared memory 
    if ((fd_shm = shm_open (sink, O_RDWR | O_CREAT, 0660)) == -1) 
        error ("shm_open");

    if (ftruncate (fd_shm, sizeof (Sink)) == -1) 
        error ("ftruncate");
            
    if ((shared_mem_ptr = mmap (NULL, sizeof (Sink), PROT_READ | PROT_WRITE, MAP_SHARED, fd_shm, 0)) == MAP_FAILED) 
        error ("mmap");
        
    if(isInit)
    {
        memset(shared_mem_ptr, 0, sizeof (Sink));
        printf("reset\n");
    }
        
    
    // TODO: Initialize the shared memory values
    int i=0;
    for(; i<MaxConsumersCount; i++)
    {
        if(!strlen(shared_mem_ptr->ports[i]) || strcmp(shared_mem_ptr->ports[i], port)==0)
        {
            portNumber = i;
            strcpy(shared_mem_ptr->ports[i], port);
            shared_mem_ptr->openedPorts[i] = 1;
            break;
        }
    }

    if(portNumber == -1)
        error("sink is full");

    // counting semaphore, indicating the number of available buffers. Initial value = MAX_BUFFERS 
    if ((buffer_count_sem = sem_open (SEM_BUFFER_COUNT_NAME, O_CREAT, 0660, MAX_BUFFERS)) == SEM_FAILED) 
        error ("sem_open");
    
    // counting semaphore, indicating the number of strings to be printed. Initial value = 0 
    if ((spool_signal_sem = sem_open (SEM_SPOOL_SIGNAL_NAME, O_CREAT, 0660, 0)) == SEM_FAILED) 
        error ("sem_open");
    
    // Initialization complete; now we can set mutex semaphore as 1 to 
    // indicate shared memory segment is available 
    if (sem_post (mutex_sem) == -1) 
        error ("sem_post: mutex_sem");    

    // Initialization complete; now we can set mutex semaphore as 1 to 
    // indicate shared memory segment is available 
    if (sem_post (mutex_sem) == -1) 
        error ("sem_post: mutex_sem");
    shared_mem_ptr->consumersCount++;
    printf("consumers Count %i\n",shared_mem_ptr->consumersCount);
}

void read_mem(char * buffer)
{
    //in thinking of  sink organization I need to introduce struct for sink instead of flain text
    //first problem for plain is : can not recognize own messages
    //second : can not remove messages already sent by consumers
    //consumers count is unknown to do destroy before last consumer leave.
    //Reference : https://www.softprayog.in/programming/interprocess-communication-using-posix-shared-memory-in-linux

    int i =0;
    for(; i<MaxConsumersCount; i++)
    {
            if(i == portNumber || !shared_mem_ptr->openedPorts[i])
                continue;

            long writeCount = shared_mem_ptr->writeCount[i];
            long readCount = shared_mem_ptr->readCount[i][portNumber];            
            int bufferLength = writeCount - readCount; //looks like you lost some data if mod affected on length.
            if(bufferLength > 0)
            {
                int toEnd = (shared_mem_ptr->readOffset[i][portNumber] + bufferLength >= DataBufferSize) ?  DataBufferSize - shared_mem_ptr->readOffset[i][portNumber] :  bufferLength;
                int fromBegin = (shared_mem_ptr->readOffset[i][portNumber] + bufferLength >= DataBufferSize) ? (shared_mem_ptr->readOffset[i][portNumber] + bufferLength - DataBufferSize) : 0;

                strncpy(buffer, shared_mem_ptr->data[i] + shared_mem_ptr->readOffset[i][portNumber], toEnd);
                //printf("i %i, buffer length %i, offset %i, to end %i, from begin %i, writeCount %i, readCount %i \n",i, bufferLength, shared_mem_ptr->readOffset[i][portNumber], toEnd, fromBegin, writeCount, readCount);
                buffer[toEnd] = 0;
                if(fromBegin > 0)
                {
                    int bufflen2= strlen(buffer);
                    strncat(buffer, shared_mem_ptr->data[i], fromBegin);
                    buffer[bufflen2 + fromBegin] = 0;
                    shared_mem_ptr->readOffset[i][portNumber] = fromBegin;
                }
                else
                {
                    shared_mem_ptr->readOffset[i][portNumber] += bufferLength;
                }
            }

            shared_mem_ptr->readCount[i][portNumber] += strlen(buffer);
    }
}

void write_mem(const char * buffer)
{
    int len = strlen(buffer);
    if(len > DataBufferSize)
        error("it is not possible to send more data when buffer length");
    
    int toEnd = (shared_mem_ptr->writeOffset[portNumber] + len >= DataBufferSize) ? DataBufferSize - shared_mem_ptr->writeOffset[portNumber] : len;
    int fromBegin = (shared_mem_ptr->writeOffset[portNumber] + len >= DataBufferSize) ? len - DataBufferSize + shared_mem_ptr->writeOffset[portNumber] : 0;

    strncpy(shared_mem_ptr->data[portNumber] + shared_mem_ptr->writeOffset[portNumber], buffer, toEnd);
    if(fromBegin)
    {
        strncpy(shared_mem_ptr->data[portNumber], buffer + toEnd, fromBegin);
        shared_mem_ptr->writeOffset[portNumber]  = fromBegin;
    }
    else
    {
        shared_mem_ptr->writeOffset[portNumber] += toEnd;
    }

    shared_mem_ptr->writeCount[portNumber] += len;
}

void destroy_sink()
{
    //Deleting shared memory with ipcrm in Linux
    //https://stackoverflow.com/questions/398886/deleting-shared-memory-with-ipcrm-in-linux
    //Delete all shared memory and semaphores on Linux
    //https://stackoverflow.com/questions/2143404/delete-all-shared-memory-and-semaphores-on-linux

    //todo: should be shm_unlink after finish last consumer 
    //http://man7.org/linux/man-pages/man7/shm_overview.7.html
    
    //too: ERROR! sigev
    //todo: warn unreaded here
    int unlink = 1;
    int i =0;
    for(; i < MaxConsumersCount; i++)
    {
        if(shared_mem_ptr->openedPorts[i])
        {
            unlink = 0;
        }
    }

    if(unlink)
    {
        shm_unlink (fd_shm);
    }
    else
    {
        shared_mem_ptr->openedPorts[portNumber] = 2;
        close(fd_shm);
    }
    
    sem_close (mutex_sem);
    if (munmap (shared_mem_ptr, sizeof (Sink)) == -1) 
        error ("munmap");
    sem_close(buffer_count_sem);
    sem_close(spool_signal_sem);
    shared_mem_ptr->consumersCount--;
    printf("\nGoodbye\n");
}