#include <assert.h>
#include <stdio.h>      // standard input / output functions
#include <stdlib.h>
#include <string.h>     // string function definitions
#include <unistd.h>     // UNIX standard function definitions
#include <fcntl.h>      // File control definitions
#include <termios.h>    // POSIX terminal control definitions
#include "repeater.h"

int main( int argc, char** argv )
{
    //sudo socat -d -d PTY,link=/dev/ttyS10 PTY,link=/dev/ttyS11
    
    
    printf( "this is repeater app for route messages from multiple serial port sink.\n" );
    printf( "using: a.out /dev/ttyS10 shared_memory_name [reset]\n" );
    printf( "Serial port name:\n" );
    char port[256];
    char sink_name[128];
    int isInit = 0;
    if(argc < 3)
    {
        char port_name[128];
        if(fgets(port_name, 128 , stdin))
        {
            if(strlen(port_name)<3)
                strcpy(port_name, "ttyS1");
            else
                port_name[strlen(port_name) - 1] = 0;
            getOSSpecificPortName(port_name, port);
        }
    
    
        printf( "Shared memory name for join:\n" );
        if(fgets(sink_name, 128 , stdin))
        {
            if(!strlen(sink_name))
                strcpy(sink_name, "lab2");
            else
                sink_name[strlen(sink_name) - 1] = 0;
        }
    }
    else
    {
        strcpy(port, argv[1]);
        strcpy(sink_name, argv[2]);
        if(argc>3)
            isInit = !strcmp(argv[3], "reset");
    }
    

    init_repeater(port, sink_name, isInit);
    exit( EXIT_SUCCESS );
}