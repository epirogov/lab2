
на компе есть 2 последовательных порта, с каждым портом запускается процесс, который умеет читать порт и передавать прочитанные данные в разделяемую память, а также умеет записывать в порт данные из разделяемой памяти. Демка создает разделяемую память, 2 процесса на каждый порт и связывает их между собой. В результате мы должны получить проброс портов через механизмы IPC POSIX

shm structure v.1

There is a header with data for manage shm by consumer

1. max consumers count
2. consumers count
3. data buffer size
4. registered port names array splited with semicolamn
5. locked port flags
6. closed port flags
7. last access tick per consumer
8. consumer to consumer matrix bytes send per port
9. consumer to consumer  matrix bytes read per port

Then it will be raw with arrays of data readed from every consumer port. The data size is limeted to buffer and new data will be buffered on consumer when limit exit in shm, but it should be large value to be enaught. It will be round robin with a data buffer.


How to use repeater app.


First of all, you need to compile a programm with your development tools like gcc.

1. git clone https://epirogov@bitbucket.org/epirogov/lab2
2. cd lab2
3. gcc *.h *.c -lrt -lpthread

Still a lot of warnings here, but a.out available after build.

Second you action is for make linked virttual com ports available in your sustem. You need to instal socat to do that. Than you can link your reserved ttys as in my example.

sudo socat -d -d PTY,link=/dev/ttyS10 PTY,link=/dev/ttyS11

Finally tou sould open two terminals to communicate.

terminal#1

>a.out
Port name for a chat:
ttyS10
...
my hello to S11!

terminal#2

>a.out
Port name for a chat:
ttyS11
...
glade to see you S10!

You can test the repeater with redirecting :

echo -ne '\033[2J' > /dev/ttyS10
cat -v < /dev/ttyS11


