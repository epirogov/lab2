#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "repeater.h"

#define NUM_THREADS     6

pthread_t threads[ NUM_THREADS ];
Channel_source thread_args[ NUM_THREADS ];
int pthread_index;

int create_channel(int port, void * perform)
{ 
  int result_code;
  unsigned index;
 
  // create all threads one by one
  //todo: resolve new port everytime
  thread_args[ pthread_index ].port = port;
  if(index % 2)
    thread_args[pthread_index].oposite_channel_source = &thread_args + pthread_index - 1;
  else
    thread_args[pthread_index].oposite_channel_source = &thread_args + pthread_index + 1;
  result_code = pthread_create( &threads[pthread_index], NULL, perform, &thread_args[pthread_index] );
  pthread_index++;
  return result_code;
}

int join_channels()
{ 
  // wait for each thread to complete
  int index = 0;
  for(; index < pthread_index; ++index )
  {
    // block until thread 'index' completes
    int result_code = pthread_join( threads[ index ], NULL );
    assert( !result_code );
  }

   return 0;
}