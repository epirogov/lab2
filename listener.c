#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <errno.h>   /* Error number definitions */
#include <pthread.h>
#include <time.h>
#include "repeater.h"

void* perform_listen( void* argument )
{
  Channel_source passed_in_value = *( ( Channel_source* )argument );
  int port = passed_in_value.port;
  
  /* optionally: insert more useful stuff here */

  int attenpts = 0;
  while(!passed_in_value.cancelation)
  {
    int len = readSerialPort(port);
    struct timespec tw = {0,125000000};
    struct timespec tr;
    nanosleep (&tw, &tr);
  }
 
  return NULL;
}

int readSerialPort(int port)
{
  int n = 0,
  spot = 0;
  char buf = '\0';
  /* Whole response*/
  char response[1024];
  memset(response, '\0', sizeof response);

  do {
    if(spot >= DataBufferSize)
    {
      write_mem(response);  
      memset(response, '\0', sizeof response);
      spot = 0;
    }
      
    n = read( port, &buf, 1 );
    if(n == 0)
      return 0;
    if(strlen(response)+n >=1023)
      break;
    sprintf( &response[spot], "%c", buf );
    spot += n;
  } while(n > 0);

  if(strlen(response))
  {
    write_mem(response);
  }
    
  return spot;
}