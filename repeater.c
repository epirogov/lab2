#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <pthread.h>
#include <signal.h>
#include "repeater.h"

void handler(int sig) {  
  destroy_sink();
  exit(0);
}

void init_repeater(const char * port, const char * sink, int isInit)
{
  create_sink(port, sink, isInit);
  signal(SIGINT, handler);
  
  int serialPort = openSerialPort(port);
  int result = create_channel(serialPort, create_input);
  if(result)
  {
      printf("Error: input not started\n");
      return;
  }

  result = create_channel(serialPort, perform_listen);
  if(result)
  {
    printf("Error: listening not started\n");
    return;
  }

  join_channels();
  close(serialPort);
}

