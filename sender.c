#include <stdio.h>      // standard input / output functions
#include <stdlib.h>
#include <string.h>     // string function definitions
#include <unistd.h>     // UNIX standard function definitions
#include <fcntl.h>      // File control definitions
#include <errno.h>      // Error number definitions
#include <termios.h>    // POSIX terminal control definitions
#include <pthread.h>
#include <time.h>
#include "repeater.h"

#define BUFFERSIZE     256

void create_input( void* argument )
{
    Channel_source passed_in_value = *( ( Channel_source* )argument );
    int port = passed_in_value.port;
    char message [DataBufferSize+1] = {0};
    while(!passed_in_value.cancelation)
    {
        //todo: this is good idea tosenddireactly from shm without copy
        read_mem(message);
        message [DataBufferSize] = 0;
        if(strlen(message))
        {
            //pthread_mutex_lock(passed_in_value.lock);
            sendMessage(port, message);
            printf("%s\n", message);
            memset(message, 0, DataBufferSize);
            //pthread_mutex_unlock(passed_in_value.lock);
            //todo: semaphore
        }
        struct timespec tw = {0,125000000};
        struct timespec tr;
        nanosleep (&tw, &tr);
    }
}

int sendMessage(int port, const char* text)
{
    int n_written = 0,spot = 0;
    n_written = write(port, text, strlen(text));     
    if(n_written < 0)
        printf("Error: Message not sent \n");
}